package klaagpaal

import (
  "fmt"
  "database/sql"
_ "github.com/mattn/go-sqlite3"
)

type Incident struct {
  ID        int
  Titel     string
  Tijdstip  string
  Inhoud    string
  Redacteur string
  Wijk      string
  Straat    string
  Stad      string
  Sensor    string
}
var curid int
func NextIncident( kp *sql.DB ) ( incidentToReport Incident, err error ) {
    if curid == 0 { // first time here
       if curid, err = maxVerwerktIncident( kp ); err != nil {
         curid = 0
       }
    }
    fmt.Printf( "NextIncident: Looking for incident id's > %d\n", curid )
    if incidentToReport, err = VolgendeIncident( kp, curid ); err != nil {
       if err != sql.ErrNoRows {
         fmt.Printf( "NextIncident: failed: %v\n", err )
       }
       return incidentToReport, err
    }
    curid = incidentToReport.ID
    return incidentToReport, nil
}
