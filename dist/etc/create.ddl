CREATE TABLE eigenaars
  ( id             INTEGER PRIMARY KEY AUTOINCREMENT,
    voornaam       text NOT NULL,
    achternaam     text NOT NULL,
    tussenvoegsel  text DEFAULT '',
    email          text DEFAULT '',
    adres          text DEFAULT '',
    postcode       text DEFAULT '',
    twitternaam    text DEFAULT '',
    telefoonnummer text DEFAULT '',
    wijk           text REFERENCES wijken(naam),
    stad           text DEFAULT 'Utrecht',
    actief         boolean DEFAULT 0
  );
  CREATE UNIQUE INDEX nameunique ON eigenaars(voornaam,achternaam,tussenvoegsel);

CREATE TABLE paden
  ( id       INTEGER PRIMARY KEY AUTOINCREMENT,
    sensor   text    ,
    kanaal   text    ,
    eigenaar INTEGER REFERENCES eigenaars(id)
  );

CREATE TABLE wijken
  ( id       INTEGER PRIMARY KEY AUTOINCREMENT,
    naam     text    UNIQUE NOT NULL,
    actief   boolean DEFAULT 1
  );

CREATE TABLE incidents
  ( id        INTEGER PRIMARY KEY AUTOINCREMENT,
    titel     text,
    tijdstip  text    DEFAULT CURRENT_TIMESTAMP,
    inhoud    text, 
    redacteur text,
    wijk      text    DEFAULT '' REFERENCES wijken(naam),
    stad      text    DEFAULT 'Utrecht',
    straat    text    DEFAULT '',
    sensor    text    
  );
  CREATE UNIQUE INDEX incidentunique ON incidents(titel,tijdstip,wijk,straat,stad,sensor);

CREATE TABLE sporen
  ( id       INTEGER PRIMARY KEY AUTOINCREMENT,
    bericht  text    NOT NULL,
    tijdstip text    DEFAULT CURRENT_TIMESTAMP,
    eigenaar INTEGER NOT NULL REFERENCES eigenaars(id),
    kanaal   text    ,
    sensor   text    ,
    incident INTEGER NOT NULL REFERENCES incidents(id)
  );
