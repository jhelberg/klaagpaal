package main


import (
  "fmt"
  "time"
  "net/http"
  "strings"
  "runtime"
  "os"
  "os/signal"
  "syscall"
  "log"
  "klaagpaal"
  "github.com/hegedustibor/htgo-tts"
  "database/sql"
_ "github.com/mattn/go-sqlite3"
)
func handleSlash( w http.ResponseWriter, r *http.Request ) {
    r.ParseForm()  // parse arguments, you have to call this by yourself
    fmt.Println(r.Form)  // print form information in server side
    fmt.Println("path", r.URL.Path)
    fmt.Println("scheme", r.URL.Scheme)
    fmt.Println(r.Form["url_long"])
    for k, v := range r.Form {
        fmt.Println("key:", k)
        fmt.Println("val:", strings.Join(v, ""))
    }
    fmt.Fprintf(w, "Hello there!") 
}

func doHttp() {
   http.HandleFunc("/", handleSlash) 
   err := http.ListenAndServe(":9090", nil)
   if err != nil {
      log.Fatal("ListenAndServe: ", err)
   }
   runtime.Gosched()
}

var raised1 = time.Unix( 0, 0 )
var raised2 = time.Unix( 0, 0 )
var incidentChannel = make( chan klaagpaal.Incident, 1 )
var complaintChannel = make( chan klaagpaal.Incident, 1 )

var spoken = false
var db *sql.DB

func handleSpeakoutOrSkip() {
     if time.Now().Sub( raised1 )/time.Millisecond < 500 {
       return
     }
     klaagpaal.But1handler( nil )
     raised1 = time.Now()
     if spoken {
       spoken = false
       klaagpaal.Ledoff()
       klaagpaal.Panicledoff()
       inc := <- complaintChannel // forget about it
       klaagpaal.VoegSpoorToe( db, "", 0, 0, inc.ID )
       speech := htgotts.Speech{Folder: "audio", Language: "nl"}
       speech.Speak( "Overgeslagen" )
     } else {
       if len( incidentChannel ) == 1 {
         incident := <-incidentChannel
         klaagpaal.Ledoff()
         klaagpaal.Ledblue()
         if incident.ID == 0 {
            klaagpaal.Ledoff()
            klaagpaal.But1handler( handleSpeakoutOrSkip )
            return
         }
         if tekst, err := klaagpaal.Rits( "templates/speakout.in.txt", incident.ID, db ); err != nil {
            fmt.Printf( "handleSpeakout: templates/speakout.in.txt failed on incident (%d), error %v\n", incident.ID, err )
            klaagpaal.Ledoff()
            klaagpaal.But1handler( handleSpeakoutOrSkip )
            return
         } else {
            speech := htgotts.Speech{ Folder: "audio", Language: "nl" }
            for _, part := range strings.Split( tekst, ". . . " ) {
              tosent := strings.Split( part, ": : : " )
              speech.Speak( tosent[ 0 ] )
              time.Sleep( 40 * time.Millisecond )
            }
            spoken = true
           if len( complaintChannel ) == 0 {
             complaintChannel <- incident
             fmt.Printf( "handleSpeakout: incident (%d) )posted on complaintChannel\n", incident.ID )
           } else {
             fmt.Printf( "handleSpeakout: Complaintchannel is full, unexpected, missing out on incident %d\n", incident.ID )
           }
           klaagpaal.Panicledon()
           klaagpaal.But2handler( handleSendout )
         }
       } else {
         fmt.Println( "handleSpeakout: No incident in channel, but triggered to speak out; unexpected" )
       }
     }
     klaagpaal.Ledoff()
     if len( incidentChannel ) > 0 {
       klaagpaal.Ledred()
     } 
     klaagpaal.But1handler( handleSpeakoutOrSkip )
     raised1 = time.Now()
}
func handleSendout() {
     if time.Now().Sub( raised2 )/time.Millisecond < 500 {
       return
     }
     spoken = false
     klaagpaal.But2handler( nil )
     klaagpaal.Panicledoff()
     if len( complaintChannel ) == 1 {
       incident := <-complaintChannel
       fmt.Printf( "handleSendout: Sending out complaint with id %d\n", incident.ID )
       // send out incident to all channels
       // haal alle paden voor incident.Sensor en ga over die kanalen.
       speech := htgotts.Speech{Folder: "audio", Language: "nl"}
       if kanalen, err := klaagpaal.AlleKanalen( incident.Sensor, db ); err != nil {
         fmt.Printf( "handleSendout: error getting kanalen %v\n", err )
       } else {
         klaagpaal.Ledoff()
         klaagpaal.Ledblue()
         if len( kanalen ) == 0 {
           speech.Speak( "Geen kanalen voor deze eigenaar." )
         }
         for _, kanaal := range kanalen {
           speech.Speak( kanaal )
           switch {
             case kanaal == "Tweet van Paaltje":
               if err := klaagpaal.SendATweet( incident.ID, db ); err != nil {
                 fmt.Printf( "handleSendout: Sending out tweet failed: %v\n", err )
                 continue
               }
             case kanaal == "E-mail aan eigenaar":
               if err := klaagpaal.SendAnOwnerEmail( incident.ID, db ); err != nil {
                 fmt.Printf( "handleSendout: Sending out e-mail failed: %v\n", err )
                 continue
               }
             case kanaal == "Reactie of inzien":
               if err := klaagpaal.SendAnOwnerEmailOnInfo( incident.ID, db ); err != nil {
                 fmt.Printf( "handleSendout: Sending out e-mail failed: %v\n", err )
                 continue
               }
           }
           klaagpaal.VoegSpoorToe( db, incident.Titel, 0, 0, incident.ID )
           speech.Speak( "Verzonden" )
         }
         klaagpaal.Ledoff()
         klaagpaal.Ledgreen()
       }
     } else {
       fmt.Println( "handleSendout: complaintChannel is empty, unexpected" )
     }
     if len( incidentChannel ) > 0 {
       klaagpaal.Ledoff()
       klaagpaal.Ledred()
     } else {
       klaagpaal.Ledoff()
       klaagpaal.Ledgreen()
     }
     raised2 = time.Now()
}
func handleSigInt() {
   c := make( chan os.Signal )
   signal.Notify( c, os.Interrupt, syscall.SIGINT )
   signal.Notify( c, os.Interrupt, syscall.SIGTERM )
   go func() {
      <-c
      klaagpaal.Ledoff()
      os.Exit(1)
   }()
}

func main() {
   klaagpaal.SendStartEmail()
   klaagpaal.Ledwhite()
   time.Sleep( 1000 * time.Millisecond)
   handleSigInt()
   klaagpaal.Ledoff()
   klaagpaal.ButInit()
   //go doHttp()
   klaagpaal.But1handler( handleSpeakoutOrSkip )
   klaagpaal.But2handler( handleSendout )

   if os.Getenv( "DEMO" ) == "true" {
     fmt.Printf( "DEMO mode\n" )
   }

   var err error 
   if db, err = sql.Open( "sqlite3", "db/kp.db" ); err != nil {
      fmt.Printf( "Error opening database %v\n", err )
   }
   for err == nil {
      currentIncident, err := klaagpaal.NextIncident( db )
      for err != nil && err == sql.ErrNoRows {
        fmt.Printf( "Sleeping because of no incidents anymore\n" )
        time.Sleep( 10000 * time.Millisecond )
        if len( incidentChannel ) == 0 {
          klaagpaal.Ledoff()
        }
        currentIncident, err = klaagpaal.NextIncident( db )
      }
      if err != nil {
         fmt.Printf( "Error getting next Incident: %v\n", err )
         break
      }
      if len( currentIncident.Titel ) > 0 {
        fmt.Printf( "main: Handling incident %d\n", currentIncident.ID )
        incidentChannel <- currentIncident
        klaagpaal.Ledoff()
        klaagpaal.Ledred()
      }
   }
   if err != nil {
      fmt.Printf( "Error getting next Incident: %v\n", err )
   }
   klaagpaal.Ledclose()
}
