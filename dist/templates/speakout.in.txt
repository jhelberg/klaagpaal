@sqlselect stad, replace( 
                         coalesce( '. . . , '||nullif(straat,''), ''),
                         'Prof.', 'Professor' )
                         as straat,
           case when strftime( '%m', tijdstip ) = '01'
                then 'januari'
                when strftime( '%m', tijdstip ) = '02'
                then 'februari'
                when strftime( '%m', tijdstip ) = '03'
                then 'maart'
                when strftime( '%m', tijdstip ) = '04'
                then 'april'
                when strftime( '%m', tijdstip ) = '05'
                then 'mei'
                when strftime( '%m', tijdstip ) = '06'
                then 'juni'
                when strftime( '%m', tijdstip ) = '07'
                then 'juli'
                when strftime( '%m', tijdstip ) = '08'
                then 'augustus'
                when strftime( '%m', tijdstip ) = '09'
                then 'september'
                when strftime( '%m', tijdstip ) = '10'
                then 'oktober'
                when strftime( '%m', tijdstip ) = '11'
                then 'november'
                when strftime( '%m', tijdstip ) = '12'
                then 'december'
            end
             as maand,
           ltrim( strftime( '%d', tijdstip ), '0' ) as dag,
           nullif( ltrim( strftime( '%H', tijdstip ), '0' ), '' )||' uur' as uur,
           coalesce( '. . . , '||nullif(wijk,''), '') as wijk,
           replace( replace( replace( replace( replace( replace( replace( 
                 titel,
                        'HZ_WABO', ': : : ' ),
                        '-', ', ' ),
                        '_', ', ' ),
                        'Prof.', 'Professor' ),
                        'O₃', 'ozon' ),
                        'NO₂', 'stikstof' ),
                        'µg/m³', 'microgram per kubieke meter' )
               as titel,
           replace( replace( replace( replace( replace( replace( replace( 
                 inhoud,
                        'HZ_WABO', ': : : ' ),
                        '-', ', ' ),
                        '_', ', ' ),
                        'Prof.', 'Professor' ),
                        'O₃', 'ozon' ),
                        'NO₂', 'stikstof' ),
                        'µg/m³', 'microgram per kubieke meter' )
               as inhoud
     from incidents
    where id = @param<incidentid>;
@field<stad> @field<wijk> @field<straat>:. . . @field<dag> @field<maand> @field<uur>. . . @field<titel>;. . . @field<inhoud>
@sqlend
