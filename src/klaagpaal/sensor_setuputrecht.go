package klaagpaal


     import (
       "database/sql"
     _ "github.com/mattn/go-sqlite3"
     )

     func copyIncidents ( kp *sql.DB ) { // copy external incidents into our own
/*
       if idatabase, err := gorm.Open("sqlite3", os.Getenv("KPDIR") + "sensors/" + "id.db" ); err != nil {
         panic("failed to connect database")
       }
       defer idatabase.Close()

       var last Incident
       if err := kp.Where( "sensor = ?", sensorname ).Last( &last ); err != nil {
         return
       }
       if rows, err := idatabase.Table( "incidents" ).Select( "Tijdstip > ?", last.Tijdstip ); err != nil {
         if gorm.IsRecordNotFoundError( err ) {
           return 
         } else {
           panic( fmt.Sprintf( "failed to get incidents from %s: %v", sensorname, err ) )
         }
       } else {
          for rows.Next() {
             var new Incident
             err := rows.Scan( &new.ID, &new.Titel, &new.Tijdstip, 
                               &new.Inhoud, &new.Redacteur,
                               &new.Wijk, &new.Stad )
             new.ID = nil
             new.Sensor = sensorname
             kp.Save( new )
          }
       }
*/
     }

     func nnNextIncident( kp *sql.DB ) (Incident, error) {
       copyIncidents( kp )
/*
       // fetch latest handled incident from this sensor
       // select incident from sporen s where s.sensor = "setup utrecht" order by id DESC limit 1
       var latest Sporen
       var latestIncidentId int
       if err := kp.Where( "sensor = ?" , sensorname ).Last(&latest); gorm.IsRecordNotFoundError( err ) {
         // no previous handled incident found, do this one
         latestIncidentId = 0
       } else if latest.sensor != nil && latest.sensor == sensorname { // fetch the incident
         latestIncidentId = latest.incident
       }

       var incidentToReport Incident
       if err := kp.Where( "id > ?", latestIncidentId ).First( &incidentToReport ); gorm.IsRecordNotFoundError( err  ) {
         return nil, nil
       } else {
         return incidentToReport, nil
       }
*/
       var incidentToReport Incident
       incidentToReport.ID    = 1
       incidentToReport.Titel =  "Kliko's versperren voetpad"
       incidentToReport.Tijdstip = "2018-06-01T12:33:00" 
       incidentToReport.Inhoud = "Aan de Prof. Ritzema-Boslaan, ter hoogte van nummer 22, staan 8 kliko's op het voetpad en stremmen daarmee de doorgang voor voetgangers, slechtzienden en rolstoelgebruikers."
       incidentToReport.Redacteur = "Joost Helberg @ Setup Utrecht"
       incidentToReport.Wijk = "Tuindorp"
       incidentToReport.Stad = "Utrecht"
       incidentToReport.Sensor = "setuputrecht"
       return incidentToReport, nil
     }
