package klaagpaal


    import (
//      "fmt"
//     "time"
      "github.com/davecheney/gpio"
    )
    var red, green, blue gpio.Pin
    var panicled gpio.Pin
    var lerr error

    func LedInit() {
      if red == nil || green == nil || blue == nil {
         red,     lerr = gpio.OpenPin( 17, gpio.ModeOutput )
         if lerr == nil { 
           green, lerr = gpio.OpenPin( 27, gpio.ModeOutput )
         }
         if lerr == nil {
           blue,  lerr = gpio.OpenPin( 22, gpio.ModeOutput )
         }
         if lerr == nil {
           panicled,  lerr = gpio.OpenPin( 25, gpio.ModeOutput )
         }
      }
      if lerr != nil && (red == nil || green == nil || blue == nil || panicled == nil) {
        panic( lerr )
      }
    }
    func Ledclose() {
       LedInit()
       Ledoff()
       red.Close()
       green.Close()
       blue.Close()
       panicled.Close()
    }
    func Ledoff() {
       LedInit()
       red.Clear()
       green.Clear()
       blue.Clear()
    }
    func Ledwhite() {
       LedInit()
       red.Set()
       green.Set()
       blue.Set()
    }
    func Ledgreen() {
       LedInit()
       green.Set()
    }
    func Ledblue() {
       LedInit()
       blue.Set()
    }
    func Ledred() {
       LedInit()
       red.Set()
    }
    func Panicledon() {
       LedInit()
       panicled.Set()
    }
    func Panicledoff() {
       panicled.Clear()
    }
