@sqlselect sensor, stad, tijdstip, redacteur, straat, wijk, titel, inhoud, (select email from eigenaars where actief limit 1) as rcpto
     from incidents
    where id = @param<incidentid>;@comment
To: @field<rcpto>
From: Paaltje <paaltje@setup.nl>
Subject: @field<sensor> -- @field<titel>

Tijdstip: @field<tijdstip>
@field<stad> -- @field<wijk> -- @field<straat>
@field<redacteur>
@field<titel>
@field<inhoud>
@sqlend
