package klaagpaal

  import (
    "os"
    "fmt"
    "strconv"
    "database/sql"
    "io/ioutil"
    "gitlab.com/jhelberg/reducer/src/reducer"
  )

func Rits( template string, incidentid int, dbconn *sql.DB ) (string, error) {
    var content string
    if bytes, err := ioutil.ReadFile( template ); err != nil {
      fmt.Fprintln(os.Stderr, fmt.Sprintf( "Reading twitter template %s failed (%v)", template, err ) )
      return "", err
    } else {
      content = string( bytes )
    }
    bla := reducer.NewReducer()
    bla.NamedParameters[ "incidentid" ] = strconv.Itoa( incidentid )
    if reduced, err := bla.Reduce( content, dbconn ); err != nil {
      return "", err
    } else {
      return reduced, nil
    }
}
