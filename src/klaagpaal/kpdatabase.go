package klaagpaal

   import (
     "fmt"
     "database/sql"
     "os"
   _ "github.com/mattn/go-sqlite3"
   )
func VoegSpoorToe( db *sql.DB, message string, owner int, channel int, incident int ) error {
   fmt.Printf( "Leaving trace [%s] for incident %d, channel %d\n", message, incident, channel )
   if stmnt, err := db.Prepare( "insert into sporen (bericht, eigenaar, kanaal, incident) values (?,?,?,?);" ); err != nil {
     return err
   } else {
     if _, err := stmnt.Exec( message, owner, channel, incident ); err != nil {
       return err
     }
   }
   return nil
}
func maxVerwerktIncident( db *sql.DB ) ( curid int, err error ) {
  query := ""
  if os.Getenv( "DEMO" ) == "true" {
    fmt.Printf( "Selecting last incident in DEMO mode\n" )
    query = "select max(incident) - 11 + cast(round(10*abs(random())/9223372036854775808) as int) from sporen;"
  } else {
    query = "select max( incident ) from sporen;"
  }
  if err := db.QueryRow( query ).Scan( &curid ); err != nil {
     curid = 0
  }
  return
}

func VolgendeIncident( db *sql.DB, curid int ) ( Incident, error ) {
    var incidentToReport Incident
    var sqlID        sql.NullInt64
    var sqlTitel     sql.NullString
    var sqlTijdstip  sql.NullString
    var sqlInhoud    sql.NullString
    var sqlRedacteur sql.NullString
    var sqlWijk      sql.NullString
    var sqlStraat    sql.NullString
    var sqlStad      sql.NullString
    var sqlSensor    sql.NullString
    query, err := db.Prepare( "select id, titel, tijdstip, inhoud, redacteur, wijk, straat, stad, sensor from incidents where id > ? order by id limit 1;" )
    if err != nil {
       fmt.Printf( "Nextincident: Error selecting incident id's > %d: %v\n", curid, err )
       return incidentToReport, err
    }
    if err = query.QueryRow( curid ).Scan( &sqlID,
                                           &sqlTitel,
                                           &sqlTijdstip,
                                           &sqlInhoud,
                                           &sqlRedacteur,
                                           &sqlWijk,
                                           &sqlStraat,
                                           &sqlStad,
                                           &sqlSensor  ); err != nil  {
       return incidentToReport, err
    }
    if sqlID.Valid {
        incidentToReport.ID = int( sqlID.Int64 )
    }
    if sqlTitel.Valid {
        incidentToReport.Titel = sqlTitel.String
    }
    if sqlTijdstip.Valid {
        incidentToReport.Tijdstip = sqlTijdstip.String
    }
    if sqlInhoud.Valid {
        incidentToReport.Inhoud = sqlInhoud.String
    }
    if sqlRedacteur.Valid {
        incidentToReport.Redacteur = sqlRedacteur.String
    }
    if sqlWijk.Valid {
        incidentToReport.Wijk = sqlWijk.String
    }
    if sqlStraat.Valid {
        incidentToReport.Straat = sqlStraat.String
    }
    if sqlStad.Valid {
        incidentToReport.Stad = sqlStad.String
    }
    if sqlSensor.Valid {
        incidentToReport.Sensor = sqlSensor.String
    }
    return incidentToReport, nil
}

func EmailVanEigenaar( db *sql.DB ) ( string, error ) {
    query, err := db.Prepare( "select email from eigenaars where actief limit 1;" )
    if err != nil {
       fmt.Printf( "EmailVanEigenaar: Error selecting owners e-mail: %v\n", err )
       return "", err
    }
    var email string
    if err = query.QueryRow().Scan( &email ); err != nil  {
       return "", err
    }
    return email, nil
}

func AlleKanalen( sensor string, db *sql.DB ) ( []string, error ) {
    query, err := db.Prepare( "select kanaal from paden where sensor = ? and eigenaar = (select id from eigenaars where actief limit 1)")
    if err != nil {
       fmt.Printf( "AlleKanalen: Error selecting kanalen: %v\n", err )
       return nil, err
    }
    kanalen := make( []string, 0, 100 )
    if rows, err := query.Query( sensor ); err != nil  {
       return nil, err
    } else {
       for rows.Next() {
         var kanaal string
         if err := rows.Scan( &kanaal ); err != nil {
           return kanalen, err
         }
         kanalen = append( kanalen, kanaal )
       }
       return kanalen, nil
    }
}
