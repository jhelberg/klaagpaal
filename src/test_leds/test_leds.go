package main


      import (
        "fmt"
        "time"
//        "os"
        "github.com/stianeikeland/go-rpio"
      )

    func ledon( p rpio.Pin ) {
      fmt.Printf( "Pin: %v", p )
      p.Output()
      p.High()
      time.Sleep( 1500 * time.Millisecond)
      p.Low()
      fmt.Printf( "     %v\n", p )
      time.Sleep( 1500 * time.Millisecond)
    }

    func main() {
         err := rpio.Open()
         if err == nil {
           // ledon( rpio.Pin( 1 ) )
           // ledon( rpio.Pin( 2 ) )
           // ledon( rpio.Pin( 3 ) )
           // ledon( rpio.Pin( 4 ) )
           // ledon( rpio.Pin( 5 ) )
           // ledon( rpio.Pin( 6 ) )
           // ledon( rpio.Pin( 7 ) )
           // ledon( rpio.Pin( 8 ) )
           // ledon( rpio.Pin( 9 ) )
           // ledon( rpio.Pin( 10 ) )
           // ledon( rpio.Pin( 11 ) )
           // ledon( rpio.Pin( 12 ) )
           // ledon( rpio.Pin( 13 ) )
           // ledon( rpio.Pin( 14 ) )
           // ledon( rpio.Pin( 15 ) )
           // ledon( rpio.Pin( 16 ) )
           ledon( rpio.Pin( 17 ) )
           // ledon( rpio.Pin( 18 ) )
           // ledon( rpio.Pin( 19 ) )
           // ledon( rpio.Pin( 20 ) )
           // ledon( rpio.Pin( 21 ) )
           ledon( rpio.Pin( 22 ) )
           // ledon( rpio.Pin( 23 ) )
           // ledon( rpio.Pin( 24 ) )
           // ledon( rpio.Pin( 25 ) )
           // ledon( rpio.Pin( 26 ) )
           ledon( rpio.Pin( 27 ) )
         }
    }
