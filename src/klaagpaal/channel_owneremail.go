package klaagpaal

import (
      "os"
      "os/exec"
      "fmt"
      "net/smtp"
      "database/sql"
)

func SendAnOwnerEmail( incidentid int, dbconn *sql.DB ) (err error) {
   return reduceAndSendEmail( incidentid, "templates/email-eigenaar.in.txt", dbconn )
}

func SendAnOwnerEmailOnInfo( incidentid int, dbconn *sql.DB ) (err error) {
   return reduceAndSendEmail( incidentid, "templates/email-info-react.in.txt", dbconn )
}

func SendStartEmail() {
   fmt.Printf( "Sendout of start email to joost\n" )

   addition := ""
   out, err := exec.Command("/sbin/ip", "addr").Output()
   if err != nil {
      fmt.Printf("Error running ip addr : %v\n", err)
      addition = ""
   } else {
      addition = string( out ) + "\r\n"
   }

   addition = addition + "DEMO: [" + os.Getenv( "DEMO" ) + "]\r\n"

   hostname := "htip.helberg.nl"
   auth := smtp.PlainAuth( "", "paaltje", os.Getenv( "SMTP_PASSWORD" ), hostname )
   to := []string{ "joost73721@helberg.nl" }
   msg := []byte("To: Joost Helberg <joost73721@helberg.nl>\r\n" +
	   "Subject: paaltje34 started\r\n" +
	   "\r\n" + addition + "\r\n" +
	   "done.\r\n" )


   if err := smtp.SendMail( hostname + ":587", auth, "paaltje@setup.nl", to, msg ); err != nil {
     fmt.Printf( "Sendout of start email failed with %v\n", err )
   }
   fmt.Printf( "Sendout of start email OK\n" )
}

func reduceAndSendEmail( incidentid int, template string, dbconn *sql.DB ) (err error) {
   if reduced, err := Rits( template, incidentid, dbconn ); err != nil {
     return err
   } else {
     fmt.Fprintln( os.Stderr, fmt.Sprintf( "Reduced content for OwnerEmail is: %s\n", reduced ) )
     hostname := "htip.helberg.nl"
     auth := smtp.PlainAuth( "", "paaltje", os.Getenv( "SMTP_PASSWORD" ), hostname )
     if email, err := EmailVanEigenaar( dbconn ); err != nil {
       return err
     } else {
       to := []string{ email }
       if err := smtp.SendMail( hostname + ":587", auth, "paaltje@setup.nl", to, []byte( reduced ) ); err != nil {
         return err
       }
     }
   }
   return nil
}
