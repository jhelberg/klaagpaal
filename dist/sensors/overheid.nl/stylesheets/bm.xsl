<?xml version="1.0" encoding="utf-8"?>
     <xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                     xmlns:srw="http://www.loc.gov/zing/srw/"
                     xmlns:sru="http://standaarden.overheid.nl/sru"
                     xmlns:overheidbm="http://standaarden.overheid.nl/bm/terms/"
                     xmlns:dcterms="http://purl.org/dc/terms/"
       version="1.0">

     <xsl:output method="text" indent="yes" encoding="utf-8"/>

     <xsl:template match="text()"/>

     <xsl:template match="srw:recordData | recordData">
         <xsl:text>insert into incidents (titel, tijdstip, stad, wijk, straat, sensor) values (</xsl:text>
         <xsl:text>'</xsl:text><xsl:apply-templates select="sru:gzd/sru:originalData/overheidbm:meta/overheidbm:owmskern/dcterms:title"/><xsl:text>',</xsl:text>
         <xsl:text>'</xsl:text><xsl:apply-templates select="sru:gzd/sru:originalData/overheidbm:meta/overheidbm:owmskern/dcterms:modified"/><xsl:text>',</xsl:text>
         <xsl:text>'</xsl:text><xsl:apply-templates select="sru:gzd/sru:enrichedData/sru:woonplaatsAdres/sru:woonplaats"/><xsl:text>',</xsl:text>
         <xsl:text>'',</xsl:text>
         <xsl:text>'</xsl:text><xsl:apply-templates select="sru:gzd/sru:enrichedData/sru:woonplaatsAdres/sru:straat"/><xsl:text>',</xsl:text>
         <xsl:text>'bmak'</xsl:text>
         <xsl:text>);
</xsl:text>
     </xsl:template>
   
     <xsl:template match="sru:woonplaats | woonplaats">
       <xsl:value-of select="text()"/><xsl:text> </xsl:text>
     </xsl:template>

     <xsl:template match="sru:straat | straat">
       <xsl:value-of select="text()"/><xsl:text> </xsl:text>
     </xsl:template>

     <xsl:template match="dcterms:title | title">
       <xsl:value-of select="text()"/>
     </xsl:template>

     <xsl:template match="dcterms:modified | modified">
       <xsl:value-of select="text()"/>
     </xsl:template>

     </xsl:stylesheet>
