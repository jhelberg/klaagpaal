<?xml version="1.0" encoding="utf-8"?>
     <xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                     xmlns:ovd="http://explain.z3950.org/dtd/2.0/"
       version="1.0">

     <xsl:output method="text" indent="yes" encoding="utf-8"/>

     <xsl:template match="text()"/>

     <xsl:template match="ovd:name | name">
       <xsl:value-of select="text()"/>
       <xsl:text>	</xsl:text><xsl:value-of select="@set"/>
     </xsl:template>

     <xsl:template match="ovd:index | index">
       <xsl:apply-templates/>
       <xsl:choose>
         <xsl:when test="@search='true'">
           <xsl:text>	yes</xsl:text>
         </xsl:when>
         <xsl:otherwise>
           <xsl:text>	no</xsl:text>
         </xsl:otherwise>
       </xsl:choose>
       <xsl:choose>
         <xsl:when test="@sort='true'">
           <xsl:text>	yes</xsl:text>
         </xsl:when>
         <xsl:otherwise>
           <xsl:text>	no</xsl:text>
         </xsl:otherwise>
       </xsl:choose>
       <xsl:text>
</xsl:text>
     </xsl:template>
     </xsl:stylesheet>
