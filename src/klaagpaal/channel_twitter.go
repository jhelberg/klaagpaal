package klaagpaal

 import (
       "os"
       "fmt"
       "github.com/dghubble/go-twitter/twitter"
       "github.com/dghubble/oauth1"
       "database/sql"
 )

 func SendATweet( incidentid int, dbconn *sql.DB ) error {
   if reduced, err := Rits( "templates/twitter-@paaltje.in.txt", incidentid, dbconn ); err != nil {
     return err
   } else {
     fmt.Fprintln( os.Stderr, fmt.Sprintf( "Reduced content for tweet is: %s\n", reduced ) )
     config := oauth1.NewConfig("0bS3bcE3Sw2LaRACHknF02nOE", 
                                os.Getenv( "CONSUMER_SECRET" ) )
     token  := oauth1.NewToken("1007548648895516673-CZxR4pZ0rf2KCghHIEI3WKcDE96st1", 
                                os.Getenv( "ACCESS_TOKEN_SECRET" ) )
     httpClient := config.Client(oauth1.NoContext, token)
     client := twitter.NewClient(httpClient)
     _, _, err := client.Statuses.Update( reduced, nil )
     return err
  }
}
