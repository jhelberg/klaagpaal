package main


      import (
//        "fmt"
//       "time"
        "github.com/davecheney/gpio"
      )
var red, green, blue gpio.Pin
var err error

    func LedInit() {
      if red == nil || green == nil || blue == nil {
        red, err   = gpio.OpenPin( 22, gpio.ModeOutput )
        green, err = gpio.OpenPin( 17, gpio.ModeOutput )
        blue, err  = gpio.OpenPin( 18, gpio.ModeOutput )
      }
    }
    func Ledclose() {
       red.Close()
       green.Close()
       blue.Close()
    }
    func Ledoff() {
       if err == nil {
         red.Clear()
         green.Clear()
         blue.Clear()
       } else {
         panic( err )
       }
    }
    func Ledgreen() {
       if err == nil {
         green.Set()
       } else {
         panic( err )
       }
    }
    func Ledblue() {
       if err == nil {
         blue.Set()
       } else {
         panic( err )
       }
    }
    func Ledred() {
       if err == nil {
         red.Set()
       } else {
         panic( err )
       }
    }
