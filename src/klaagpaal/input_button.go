package klaagpaal


  import (
    "fmt"
    "time"
    "github.com/davecheney/gpio"
  )
var but1 gpio.Pin
var but2 gpio.Pin
var berr error
var raised = time.Unix( 0, 0 )

func raise() {
   if time.Now().Sub( raised )/time.Millisecond < 500 {
     return
   }
   raised = time.Now()
   fmt.Printf( "Raised\n")
}
func ButInit() {
  if but1 == nil {
     but1, berr  = gpio.OpenPin( 5, gpio.ModeInput )
     but1.BeginWatch( gpio.EdgeRising, raise )
  }
  if but1 == nil {
    panic( berr )
  }
  if but2 == nil {
     but2, berr  = gpio.OpenPin( 16, gpio.ModeInput )
     but2.BeginWatch( gpio.EdgeRising, raise )
  }
  if but2 == nil {
    panic( berr )
  }
}
func But1handler( handler gpio.IRQEvent ) {
     if handler == nil {
       but1.BeginWatch( gpio.EdgeRising, raise )
     } else {
       but1.BeginWatch( gpio.EdgeRising, handler )
     }
}
func But2handler( handler gpio.IRQEvent ) {
     if handler == nil {
       but2.BeginWatch( gpio.EdgeRising, raise )
     } else {
       but2.BeginWatch( gpio.EdgeRising, handler )
     }
}
